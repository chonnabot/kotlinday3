package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.UserStatus

data class CustomerNDto(
        var name: String? = null,
        var email: String? = null,
        var username: String? = null,
        var authorities: List<AuthorityDto> = mutableListOf()
        )