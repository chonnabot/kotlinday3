package dv.spring.kotlin.entity.dto

data class AddCustomerDto(
        var id: Long? = null
)
