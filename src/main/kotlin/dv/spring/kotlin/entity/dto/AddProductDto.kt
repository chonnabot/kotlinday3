package dv.spring.kotlin.entity.dto

data class AddProductDto(
        var id: Long? = null
)
