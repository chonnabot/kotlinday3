package dv.spring.kotlin.entity.dto

data class AddSelectedProductDto(
        var quantity: Int? = null,
        var products: AddProductDto? = null
)
