package dv.spring.kotlin.entity.dto

data class AddShoppingCartDto(
        var customer: AddCustomerDto? = null,
        var selectedProduct: List<AddSelectedProductDto> = mutableListOf<AddSelectedProductDto>()
)
