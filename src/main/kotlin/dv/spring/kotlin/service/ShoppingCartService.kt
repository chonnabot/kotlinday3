package dv.spring.kotlin.service

import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.dto.AddShoppingCartDto
import org.springframework.data.domain.Page

interface ShoppingCartService {
    fun getShoppingCarts(): List<ShoppingCart>
    fun getShoppingCartByProductName(name: String): List<ShoppingCart>
    fun getShoppingCartByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getCustomerByProduct(name: String): List<ShoppingCart>
    fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun save(mapShoppingCartDto: AddShoppingCartDto): ShoppingCart

}