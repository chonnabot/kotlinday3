package dv.spring.kotlin.service

import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.dao.ProductDao
import dv.spring.kotlin.dao.SelectedProductDao
import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.dto.AddShoppingCartDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ShoppingCartServiceImpl : ShoppingCartService {
    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao
    @Autowired
    lateinit var selectedProductDao: SelectedProductDao
    @Autowired
    lateinit var customerDao: CustomerDao
    @Autowired
    lateinit var productDao: ProductDao


    override fun save(mapShoppingCartDto: AddShoppingCartDto): ShoppingCart {
        val selected = mutableListOf<SelectedProduct>()
        for (item in mapShoppingCartDto.selectedProduct) {
            var product = productDao.findById(item.products!!.id!!)
            selected.add(selectedProductDao.save(item.quantity!!, product))
        }
        val customer = customerDao.findById(mapShoppingCartDto.customer!!.id!!)
        val shoppingCarts = ShoppingCart()
        shoppingCarts.customer = customer!!
        shoppingCarts.selectedProduct = selected!!
        shoppingCartDao.save(shoppingCarts)
        return shoppingCarts

    }

    override fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartWithPage(page, pageSize)
    }

    override fun getCustomerByProduct(name: String): List<ShoppingCart> {
        return shoppingCartDao.getCustomerByProduct(name)
    }

    override fun getShoppingCartByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartByProductNameWithPage(name, page, pageSize)
    }

    override fun getShoppingCartByProductName(name: String): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCartByProductName(name)
    }

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCarts()
    }
}