package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.entity.dto.CustomerDto

interface CustomerDao {
    fun getCustomers(): List<Customer>
    fun getCustomerByName(name:String):Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndDesc(name: String, email: String): List<Customer>
    fun getCustomerByAddress(name: String): List<Customer>
    fun getCustomerByStatus(status: UserStatus): List<Customer>
    fun save(mzpCustomerDto: Customer): Customer
    fun findById(id: Long): Customer?
}